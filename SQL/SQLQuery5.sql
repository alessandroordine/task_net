--CREATE TABLE Studente(
--	studenteID INTEGER PRIMARY KEY IDENTITY(1,1),
--	nominativo VARCHAR(500)NOT NULL,
--	matricola VARCHAR(25) UNIQUE NOT NULL,
--);

--CREATE TABLE Esame(
--	esameID INTEGER PRIMARY KEY IDENTITY(1,1),
--	nome_esame VARCHAR(250) NOT NULL,
--	crediti INTEGER NOT NULL,
--	data_esame DATETIME NOT NULL
--);

--CREATE TABLE Studente_Esame(
--	studenteRIF INTEGER NOT NULL,
--	esameRIF INTEGER NOT NULL,
--	FOREIGN KEY (studenteRIF) REFERENCES Studente(studenteID),
--	FOREIGN KEY (esameRIF) REFERENCES ESAME(esameID),
--	UNIQUE(studenteRIF, esameRIF)						--PRIMARY KEY (studenteRIF, esameRIF) occupa molto di pi� di  UNIQUE(studenteRIF, esameRIF)
--);

--INSERT INTO Studente (nominativo, matricola) VALUES
--('Giovanni Pace', '123456'),
--('Mario Rossi', '123457'),
--('Valeria Verdi', '123458'),
--('Maria Nardi', '123459');

--INSERT INTO Esame (nome_esame, crediti, data_esame) VALUES
--('Analisi',6, '2022-03-19T090:00:00'),
--('Fisica',6, '2022-03-20T090:00:00'),
--('Informatica',9, '2022-03-21T090:00:00'),
--('Sistemi',6, '2022-03-19T090:00:00'),
--('Elettronica',6, '2022-03-19T090:00:00');

--SELECT * FROM Studente;
--SELECT * FROM Esame;

INSERT INTO Studente_Esame (studenteRIF, esameRIF) VALUES
(1,2),
(1,4),
(2,2),
(2,3),
(3,2),

SELECT * FROM Studente;
SELECT * FROM Esame;
SELECT * FROM Studente_Esame;

SELECT * FROM Studente
	JOIN Studente_Esame ON Studente.studenteID = Studente_Esame.studenteRIF;