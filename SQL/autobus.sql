
--  TASK
--Societ� di autobus urbana che gestisce le linee di una citt�. Ogni linea di bus ha un suo numero 
--identificativo e per ognuna di esse � previsto un certo numero di corse che si ripetono quotidianamente 
--secondo un stesso orario che � diverso per i giorni feriali e festivi.
--La rete di trasporti prevede una serie di fermate, la cui identificazione avviene mediante il nome della via della fermata. 
--Ciascuna fermata pu� essere utilizzata da pi� linee. Di ogni linea � specificato il percorso 
--attraverso le fermate che compie e per ciascuna di esse � nota la distanza dal capolinea.
-- La societ� impiega autisti, di cui conosce i dati anagrafici e lo stipendio mensile, e possiede una serie di autobus, ciascuno dei quali
-- � identificato da un codice proprio dalla societ� e dalla targa.




CREATE TABLE Societa(

    societaID INTEGER PRIMARY KEY IDENTITY(1,1),
    codice_soc VARCHAR(250) NOT NULL UNIQUE,
    rag_sociale VARCHAR(500) NOT NULL UNIQUE,
    citta VARCHAR(250)

);

CREATE TABLE Linea(
    lineaID INTEGER PRIMARY KEY IDENTITY(1,1),
    numeroLinea VARCHAR(25)
);

CREATE TABLE Corsa(

    corsaID INTEGER PRIMARY KEY IDENTITY(1,1),
    orario_standard TIME NOT NULL,
    orario_festivo TIME NOT NULL,
    lineaRIF INTEGER NOT NULL
    FOREIGN KEY(lineaRIF) REFERENCES Linea(lineaID) ON DELETE CASCADE

);

CREATE TABLE Fermata(

    fermataID  INTEGER PRIMARY KEY IDENTITY(1,1),
    nome_via VARCHAR(250) UNIQUE NOT NULL,
    numero_fermata INTEGER DEFAULT 0,
    cap INTEGER NOT NULL,
    UNIQUE(nome_via, numero_fermata, cap)

);

CREATE TABLE Fermata_Corsa(

    fermataRIF INTEGER NOT NULL,
    corsaRIF INTEGER NOT NULL,
    distanza_capo FLOAT NOT NULL DEFAULT 0,
    FOREIGN KEY(fermataRIF) REFERENCES Fermata(fermataID) ON DELETE CASCADE,
    FOREIGN KEY(corsaRIF) REFERENCES Corsa(corsaID) ON DELETE CASCADE,
    UNIQUE(fermataRIF,corsaRIF),

);

CREATE TABLE Linea_Fermata(

    fermataRIF INTEGER NOT NULL,
    lineaRIF INTEGER NOT NULL,
    capolinea BIT NOT NULL DEFAULT 0,
    FOREIGN KEY(fermataRIF) REFERENCES Fermata(fermataID) ON DELETE CASCADE,
    FOREIGN KEY(lineaRIF) REFERENCES Linea(lineaID) ON DELETE CASCADE,
    UNIQUE(fermataRIF,lineaRIF),

);

CREATE TABLE Autobus(

    autobusID INTEGER PRIMARY KEY IDENTITY(1,1),
    codiceUnivoco VARCHAR(25) NOT NULL,
    targa VARCHAR(25) NOT NULL UNIQUE,
    societaRIF INTEGER NOT NULL,
    lineaRIF INTEGER,
    FOREIGN KEY(societaRIF) REFERENCES Societa(societaID) ON DELETE CASCADE,
    FOREIGN KEY(lineaRIF) REFERENCES Linea(lineaID) ON DELETE SET NULL,
    UNIQUE(societaRIF, codiceUnivoco)  

);

CREATE TABLE Autista(

    autistaID INTEGER PRIMARY KEY IDENTITY(1,1),
    stipendio FLOAT NOT NULL DEFAULT 0,
    anagrafica VARCHAR(500),
    societaRIF INTEGER NOT NULL,
    autobusRIF INTEGER,
    FOREIGN KEY(societaRIF) REFERENCES Societa(societaID) ON DELETE CASCADE,
    FOREIGN KEY(autobusRIF) REFERENCES Autobus(autobusID) ON DELETE NO ACTION   


);