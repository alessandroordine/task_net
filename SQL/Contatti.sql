CREATE TABLE Contatti
(
	contattiID INTEGER PRIMARY KEY IDENTITY(1,1),
	num_tel INTEGER NOT NULL UNIQUE,
	cognome VARCHAR(255) NOT NULL,
	nome VARCHAR(255) NOT NULL,
);

INSERT INTO Contatti (num_tel, cognome, nome) VALUES (0655438978, 'Pasticcio','Ciccio');
INSERT INTO Contatti (num_tel, cognome, nome) VALUES (0655438988, 'Verdi','Valeria');
INSERT INTO Contatti (num_tel, cognome, nome) VALUES (0655438968, 'Sakuragi','Hanamichi');
INSERT INTO Contatti (num_tel, cognome, nome) VALUES (0655438948, 'Son','Goku');
INSERT INTO Contatti (num_tel, cognome, nome) VALUES (0655438998, 'Nonso','Cosa');
INSERT INTO Contatti (num_tel, cognome, nome) VALUES (0655438958, 'Scrivere','Dopo');


SELECT * FROM Contatti;
SELECT * FROM Contatti WHERE nome = 'Ciccio';
SELECT num_tel FROM Contatti;

UPDATE Contatti SET num_tel = 000 , cognome = 'Cancellato', nome = 'Completamente'
WHERE nome = 'Cosa';


UPDATE Contatti SET num_tel = 001 , cognome = 'Cancellato', nome = 'Completamente'
WHERE nome = 'Dopo';

DELETE FROM Contatti WHERE num_tel = 000;

DELETE FROM Contatti WHERE num_tel = 001;