-- DDL - Data Definition Language:
-- DML - Data Manipulation Language: operazioni di inserimento, modifica e eliminazioni di dati
-- QL  - Query Language: estrarre informazioni da una base di dati


-- Creazione tabella
--CREATE TABLE Rubrica (
--	nome VARCHAR(255),
--	cognome VARCHAR(255),
--	cod_fis VARCHAR(16),
--	telefono VARCHAR(255)
--);

-- Eliminazione tabela
-- DROP TABLE Rubrica;

-- Aggiunta di una colonna
-- ALTER TABLE Rubrica ADD Email VARCHAR(255);

--Alterazione di una colonna
--ALTER TABLE Rubrica ALTER COLUMN email VARCHAR(500);

--Eliminazione di una colonna
--ALTER TABLE Rubrica DROP COLUMN Email;

-- DML - Data Manipulation Language
--INSERT INTO Rubrica (nome, cognome, cod_fis, telefono) VALUES ('Giovanni', 'Pace', 'PCAGNN', '+39123456');

--INSERT INTO Rubrica (nome, cognome, cod_fis, telefono) VALUES
--('Mario', 'Rossi', 'MRRRSS', '+39123457'),
--('Valeria', 'Verdi', 'VLRVRD', '+39123458');

--DELETE FROM Rubrica Where nome = 'Giovanni';
DELETE FROM Rubrica Where nome = 'Mario';

-- QL - Query Language
SELECT * FROM Rubrica;

--SELECT * FROM Rubrica WHERE nome = 'Giovanni';

--SELECT * FROM Rubrica WHERE nome = 'Giovanni' AND cognome = 'Pacelli';

--DELETE FROM Rubrica WHERE cod_fis = 'MRRRSS';