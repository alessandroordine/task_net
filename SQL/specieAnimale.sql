


--a) In un giardino zoologico ci sono degli animali
--appartenenti a una specie e aventi una certa et�; 
--ogni specie � localizzata in un settore (avente un nome) dello zoo.




CREATE TABLE Settore
(
	SettoreID INTEGER PRIMARY KEY IDENTITY(1,1),
	Nome VARCHAR (255) NOT NULL
);

CREATE TABLE Specie
(
	SpecieID INTEGER PRIMARY KEY IDENTITY(1,1),
	Tipologia VARCHAR (255) NOT NULL,
	SettoreRIF INTEGER NOT NULL,
	FOREIGN KEY (SettoreRIF) REFERENCES Settore(SEttoreID)
);

CREATE TABLE Animale 
(
	animaleID INTEGER PRIMARY KEY IDENTITY(1,1),
	eta INTEGER NOT NULL,
	specieRIF INTEGER NOT NULL,
	FOREIGN KEY (specieRIF) REFERENCES Specie(SpecieID)
);

--ALTER TABLE Animale ADD Nome VARCHAR(255) NOT NULL UNIQUE;

--INSERT INTO Settore (nome) VALUES
--('Settore01'),
--('Settore02'),
--('Settore03');

INSERT INTO Specie (tipologia, settoreRIF) VALUES
('Scimmie', 1),
('Felini', 2);

INSERT INTO Animale(Nome, eta, specieRIF) VALUES
('Babbuino', 9, 2),
('Giaguaro', 8, 3),
('Gibbone', 3, 2);

SELECT COUNT (Nome) AS NumeroAnimali FROM Animale WHERE specieRIF =2;

--LEN(element_name) < value 
--definisce lunghezza del valore di un dato campo
--(se ci sono piu/meno value nella stringa  SELECT * FROM Animale WHERE LEN(nome)>2