CREATE TABLE Appunti(
	id INTEGER PRIMARY KEY IDENTITY(1,1),
	titolo VARCHAR(255) NOT NULL,
	descrizione VARCHAR(255) NOT NULL
);

INSERT INTO Appunti (titolo, descrizione) VALUES ('Lezioni musica', 'Ricorda di ripassare la terza strofa');
INSERT INTO Appunti (titolo, descrizione) VALUES ('Spesa', 'Ricorda le patate,cipolle e pollo');
INSERT INTO Appunti (titolo, descrizione) VALUES ('Boh', 'non so cosa inventarmi');
INSERT INTO Appunti (titolo, descrizione) VALUES ('Cicciopasticcio', 'decisamente autoesplicativo');

SELECT * 
	FROM Appunti
	WHERE descrizione LIKE '%Ricorda%' AND titolo LIKE '%io%';