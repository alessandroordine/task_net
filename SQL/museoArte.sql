CREATE TABLE Mostra
(
	mostraID INTEGER PRIMARY KEY IDENTITY (1,1),
	periodo VARCHAR (255) NOT NULL,
	descrizioneMostra TEXT
);

CREATE TABLE Sala
(
	salaID INTEGER PRIMARY KEY IDENTITY (1,1),
	titolo VARCHAR (255) NOT NULL,
	periodo VARCHAR (255) NOT NULL,
	oraioApertura NCHAR(5) NOT NULL,
	orarioChiusura NCHAR(5),
	descrizioneSala TEXT,
	mostraRIF INTEGER NOT NULL,
	FOREIGN KEY (mostraRIF) REFERENCES Mostra(mostraID)

);


CREATE TABLE Autore
(
	autoreID INTEGER PRIMARY KEY IDENTITY (1,1),
	nome VARCHAR (255),
	cognome VARCHAR (255),
	dataNascita DATE NOT NULL,
	luogoNascita VARCHAR (255) NOT NULL,
	biografiaAutore TEXT 
	
);


CREATE TABLE Opera
(
	operaID INTEGER PRIMARY KEY IDENTITY (1,1),
	titolo VARCHAR (255) NOT NULL,
	annoCreazione DATE NOT NULL,
	descrizioneOpera TEXT,
	autoreRIF INTEGER NOT NULL,
	salaRIF INTEGER NOT NULL,
	mostraRIF INTEGER NOT NULL,
	FOREIGN KEY (mostraRIF) REFERENCES Mostra(mostraID),
	FOREIGN KEY (autoreRIF) REFERENCES Autore(autoreID),
	FOREIGN KEY (salaRIF) REFERENCES Sala(salaID)
	
);

--DROP TABLE Opera;


--INSERT INTO Mostra (periodo, descrizioneMostra) VALUES
--('SEMPRE' , 'Museum of Modern Art di New York')

--INSERT INTO Sala (titolo, periodo, oraioApertura, orarioChiusura, descrizioneSala, mostraRIF) VALUES
--('Sala 01', 'GENNAIO', '16:00', '20:00', 'arte moderna1', 1 ),
--('Sala 02', 'FEBBRAIO', '15:00', '18:00', 'arte moderna2', 1),
--('Sala 03', 'MARZO', '10:00', '14:00', 'impressionisti', 1),
--('Sala 04', 'MARZO', '08:00', '10:00', 'impressionisti moderni', 1),
--('Sala 05', 'APRILE', '14:00', '19:00', 'espressionisti', 1),
--('Sala 06', 'MAGGIO', '15:00', '19:00', 'espressionisti moderni', 1)

--INSERT INTO Autore (nome, cognome, dataNascita, luogoNascita, biografiaAutore) VALUES 
--('Mario', 'Rossi', '1846-02-16','Bari','pittore'),
--('Valeria', 'Verdi', '1854-07-15','Bari','pittore'),
--('Ciccio', 'Pasticcio', '1845-08-09','Bari','pittore'),
--('Goku', 'Son', '1876-04-16','Bari','pittore'),
--('Hanamichi', 'Sakuragi', '1976-03-23','Bari','pittore'),
--('Idont', 'Know', '1996-10-10','Bari','pittore')

--INSERT INTO Opera (titolo, annoCreazione, descrizioneOpera,autoreRIF,salaRIF, mostraRIF) VALUES
--('capolavoro','1876-02-16','io', 3, 3, 1),
--('meh','1886-02-16','non', 1, 4, 1),
--('poteva esser meglio','1876-02-16','lo', 2, 5, 1),
--('artattack','1866-02-16','so', 4, 6, 1),
--('pokemon','1856-02-16','piu', 3, 5, 1),
--('notte stellata','1886-02-16','cosa', 2,5, 1),
--('dragon ball','2006-02-16','scrivere', 1, 1, 1),
--('slam dunk','2000-02-16','in', 5, 1, 1),
--('nemmeno io','2846-02-16','questo', 6, 2, 1),
--('lastone','9846-02-16','campo', 5, 2, 1)

--SELECT * FROM Opera WHERE salaRIF=2; 

--SELECT COUNT (annoCreazione) AS NumeroOpere FROM Opera WHERE salaRIF= 5;

--SELECT * From Mostra
--	JOIN Opera ON Mostra.mostraID = Opera.operaID
	
	--SELECT * From Mostra
	--	JOIN Opera ON mostraID=mostraRIF;

		
	
	
		
		 
		
	