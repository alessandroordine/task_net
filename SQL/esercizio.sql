CREATE TABLE Attore
(
	attoreID INTEGER PRIMARY KEY IDENTITY(1,1),
	codice INTEGER NOT NULL UNIQUE,
	cognome VARCHAR(255) NOT NULL,
	nome VARCHAR(255) NOT NULL,
	luogo_nascita VARCHAR(255),
	
		
);


CREATE TABLE Regista
(
    registaID INTEGER PRIMARY KEY IDENTITY(1,1),
	codice INTEGER NOT NULL UNIQUE,
	cognome VARCHAR(255) NOT NULL,
	nome VARCHAR(255) NOT NULL,
	luogo_nascita VARCHAR(255),
	
);

CREATE TABLE Film 
(
	filmID INTEGER  PRIMARY KEY IDENTITY(1,1),
	titolo VARCHAR (255),
	data_prod DATE,
	nazionalita VARCHAR (255),
	lingua VARCHAR (255),
	registaRIF INTEGER NOT NULL,
	FOREIGN KEY (registaRIF) REFERENCES Regista(registaID)ON DELETE NO ACTION
		
);


CREATE TABLE  Supporto 
(
	supportoID INTEGER PRIMARY KEY IDENTITY(1,1),
	tipologia VARCHAR(255),
	filmRIF INTEGER UNIQUE NOT NULL,
	FOREIGN KEY (filmRIF) REFERENCES Film (filmID) ON DELETE CASCADE
);




CREATE TABLE Persona 
(
	attoreRIF INTEGER NOT NULL,
	registaRIF INTEGER NOT NULL,
	codice INTEGER IDENTITY(1,1),
	cognome VARCHAR(255) NOT NULL,
	nome VARCHAR(255) NOT NULL,
	luogo_nascita VARCHAR(255),
	data_nascita DATE,
	FOREIGN KEY (attoreRIF) REFERENCES Attore(codiceAtt) ON DELETE CASCADE,
	FOREIGN KEY (registaRIF) REFERENCES Regista(codiceReg) ON DELETE CASCADE,
	UNIQUE(attoreRIF, registaRIF)
);

INSERT INTO Film (titolo, data_produzione, nazionalita, lingua) VALUES
('Il signore degli anelli', '2000-03-09', 'Americana', 'ENG-ITA'),
('Spider-Man', '2000-04-09', 'Americana', 'ENG-ITA'),
('Venom', '2000-05-09', 'Americana', 'ENG-ITA'),
('Avengers', '2000-06-09', 'Americana', 'ENG-ITA');

INSERT INTO Attore (cognome, nome,luogo_nascita, data_nascita) VALUES
('Giovanni', 'Pace', 'Roma', '1990-06-13'),
('Mario', 'Rossi', 'Bari', '1990-06-13'),
('Valeria', 'Verdi', 'Como', '1990-06-13'),
('Maria', 'Nardi', 'Roma', '1990-06-13');

SELECT * FROM Film;
SELECT * FROM Attore;