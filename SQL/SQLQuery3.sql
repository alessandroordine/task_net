--CREATE TABLE Attore
--(
--	attoreID INTEGER PRIMARY KEY IDENTITY(1,1),
--	codice INTEGER NOT NULL UNIQUE,
--	cognome VARCHAR(250) NOT NULL,
--	nome VARCHAR(250) NOT NULL,
--	luogo_nascita VARCHAR(250),
	
		
--);


--CREATE TABLE Regista
--(
--    registaID INTEGER PRIMARY KEY IDENTITY(1,1),
--	codice INTEGER NOT NULL UNIQUE,
--	cognome VARCHAR(250) NOT NULL,
--	nome VARCHAR(250) NOT NULL,
--	luogo_nascita VARCHAR(250),
	
--);

--CREATE TABLE Film 
--(
--	filmID INTEGER  PRIMARY KEY IDENTITY(1,1),
--	titolo VARCHAR (250) NOT NULL,
--	data_prod DATE NOT NULL,
--	nazionalita VARCHAR (250),
--	lingua VARCHAR (250),
--	registaRIF INTEGER,
--	FOREIGN KEY (registaRIF) REFERENCES Regista(registaID)ON DELETE NO ACTION		
--);

--CREATE TABLE Attore_Film
--(
--	filmRIF INTEGER NOT NULL,
--	attoreRIF INTEGER NOT NULL,
--	FOREIGN KEY (filmRIF) REFERENCES Film (filmID) ON DELETE CASCADE,
--	FOREIGN KEY (attoreRIF) REFERENCES Attore (attoreID) ON DELETE CASCADE,
--	UNIQUE(filmRIF, attoreRIF)
--);

--CREATE TABLE Supporto
--(
--	supportoID INTEGER PRIMARY KEY IDENTITY(1,1),
--	posizione VARCHAR (10) NOT NULL,
--	tipologia VARCHAR (250) NOT NULL,
--	filmRif INTEGER NOT NULL,
--	FOREIGN KEY (filmRIF) REFERENCES Film(filmID) ON DELETE CASCADE
--);

--SELECT * FROM Supporto
--	JOIN Film ON Supporto.filmRif = Film.filmID
--	LEFT JOIN Regista on Film.registaRIF = Regista.registaID
--	WHERE posizione = '123456';

--LEN(element_name) < value definisce lunghezza del valore di un dato campo(se ci sono piu/meno value nella stringa  SELECT * FROM Cantante WHERE LEN(nomeArte)>2

SELECT nome, cognome FROM Supporto
	JOIN FILM ON Supporto.filmRIF = Film.filmID
	JOIN Attore_Film ON Film.filmID = Attore_Film.filmRIF
	JOIN Attore ON Attore_Film.attoreRIF = Attore.attoreID
	WHERE posizione  = '123456';

DELETE FROM Attore_Film WHERE filmRIF = 3;

