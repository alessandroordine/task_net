function aggiungi(){
    let varTitolo = document.getElementById("inTitolo").value;
    let varAutore = document.getElementById("inAutore").value;
    let varCodice = document.getElementById("inCod").value;
    let varPrezzo = document.getElementById("inPrezzo").value;


    if(varTitolo.length == 0){
        alert("campo obbligatorio");
        document.getElementById("inTitolo").focus();
        return;
    
    
    }

    let libro = {
    titolo: varTitolo,
    autore: varAutore,
    codice: varCodice,
    prezzo: varPrezzo,
    }

    if(isPresente(libro))
        alert("Titolo già presente!");
    else{
        elenco.push(libro);

        localStorage.setItem("libreria", JSON.stringify(elenco))
        
    }   


    
}

function isPresente(varLibro){
    for(let item of elenco){
        if(item.libro == varLibro)
            return true;
    }

    return false;
}

function stampaElenco(){

    let contenuto = "";

    for(let item of elenco){

        let riga = `
            <tr>
                <td>${item.titolo}</td>
                <td>${item.autore}</td>
                <td>${item.codice}</td>
                <td>${item.prezzo}</td>             
              
            </tr>
        `;

        contenuto += riga;
    }

    document.getElementById("lista-libri").innerHTML = contenuto;

}



if(localStorage.getItem("libreria") == null)
    localStorage.setItem("libreria", JSON.stringify([]))

    let elenco = JSON.parse(localStorage.getItem("libreria"));


    stampaElenco()



