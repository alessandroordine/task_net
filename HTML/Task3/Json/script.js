function aggiungi(){
    let varId = document.getElementById("inId").value;
    let varCategoria = document.getElementById("inCategoria").value;
    let varGenere = document.getElementById("inGenere").value;
    let varProdotto = document.getElementById("inProdotto").value;
    let varPrezzo = document.getElementById("inPrezzo").value;
    let varVenduti = document.getElementById("inVenduti").value;


let prodotto = {
    id: varId,
    categoria: varCategoria,
    genere: varGenera,
    prodotto: varProdotto,
    prezzo: varPrezzo,
    venduti: varVenduti
    }
         
 let elenco = [
            {
               "id":"01",
               "categoria":"abbigliamento",
               "genere":"uomo",
               "prodotto":"maglia blu",
               "prezzo":30,
               "venduti":10
            },
            {
               "id":"02",
               "categoria":"abbigliamento",
               "genere":"donna",
               "prodotto":"maglia rossa",
               "prezzo":23,
               "venduti":13
            },
            {
               "id":"03",
               "categoria":"calzature",
               "genere":"uomo",
               "prodotto":"sneaker rosse",
               "prezzo":49,
               "venduti":4
            },
            {
               "id":"04",
               "categoria":"calzature",
               "genere":"uomo",
               "prodotto":"sneaker blu",
               "prezzo":49,
               "venduti":12
            },
            {
               "id":"05",
               "categoria":"abbigliamento",
               "genere":"uomo",
               "prodotto":"maglia collo a V",
               "prezzo":22,
               "venduti":20
            },
            {
               "id":"06",
               "categoria":"abbigliamento",
               "genere":"uomo",
               "prodotto":"tuta sport",
               "prezzo":60,
               "venduti":22
            },
            {
               "id":"07",
               "categoria":"abbigliamento",
               "genere":"uomo",
               "prodotto":"pigiama",
               "prezzo":18,
               "venduti":32
            },
            {
               "id":"08",
               "categoria":"abbigliamento",
               "genere":"donna",
               "prodotto":"pigiama",
               "prezzo":25,
               "venduti":30
            },
            {
               "id":"09",
               "categoria":"abbigliamento",
               "genere":"donna",
               "prodotto":"maglia a coste",
               "prezzo":17,
               "venduti":28
            },
            {
               "id":"10",
               "categoria":"calzature",
               "genere":"donna",
               "prodotto":"sneaker gialle",
               "prezzo":38,
               "venduti":14
            },
            {
               "id":"11",
               "categoria":"calzature",
               "genere":"donna",
               "prodotto":"classiche",
               "prezzo":60,
               "venduti":10
            },
            {
               "id":"12",
               "categoria":"calzature",
               "genere":"donna",
               "prodotto":"sneaker rosa",
               "prezzo":38,
               "venduti":23
            },
            {
               "id":"13",
               "categoria":"borse",
               "genere":"uomo",
               "prodotto":"zaino 3lt",
               "prezzo":15,
               "venduti":34
            },
            {
               "id":"14",
               "categoria":"borse",
               "genere":"uomo",
               "prodotto":"zaino 10lt",
               "prezzo":40,
               "venduti":29
            },
            {
               "id":"15",
               "categoria":"borse",
               "genere":"uomo",
               "prodotto":"monospalla",
               "prezzo":30,
               "venduti":3
            },
            {
               "id":"16",
               "categoria":"borse",
               "genere":"donna",
               "prodotto":"zainetto blu",
               "prezzo":35,
               "venduti":18
            },
            {
               "id":"16",
               "categoria":"borse",
               "genere":"donna",
               "prodotto":"zainetto rosso",
               "prezzo":40,
               "venduti":40
            },
            {
               "id":"17",
               "categoria":"borse",
               "genere":"donna",
               "prodotto":"borsa griffata",
               "prezzo":100,
               "venduti":38
            },
            {
               "id":"18",
               "categoria":"abbigliamento",
               "genere":"donna",
               "prodotto":"camicetta",
               "prezzo":15,
               "venduti":55
            },
            {
               "id":"19",
               "categoria":"abbigliamento",
               "genere":"donna",
               "prodotto":"pantaloni",
               "prezzo":18,
               "venduti":8
            },
            {
               "id":"20",
               "categoria":"abbigliamento",
               "genere":"donna",
               "prodotto":"gonna",
               "prezzo":32,
               "venduti":19
            },
            {
               "id":"21",
               "categoria":"calzature",
               "genere":"uomo",
               "prodotto":"classiche",
               "prezzo":5,
               "venduti":40
            }
         ];


         function seleziona() 
            {
                document.querySelector('#table1').classList.toggle('nascondi');
                document.querySelector('#table2').classList.toggle('nascondi');
                document.querySelector('#table3').classList.toggle('nascondi');
                document.querySelector('#table4').classList.toggle('nascondi');
                document.querySelector('#table5').classList.toggle('nascondi');

            }



        }