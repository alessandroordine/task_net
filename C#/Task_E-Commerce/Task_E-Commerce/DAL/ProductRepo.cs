﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_E_Commerce.Models;

namespace Task_E_Commerce.DAL
{
    public class ProductRepo : InterfaceRepo<Product>
    {
        private readonly IMongoCollection<Product> prodotti;

        private string strConne;
        private string strDatab;

        public ProductRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if (prodotti == null)
                prodotti = db.GetCollection<Product>("Products");

            strConne = strConnessione;
            strDatab = strDatabase;
        }
        public bool Delete(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAll()
        {
           
            List<Product> elenco = prodotti.Find(FilterDefinition<Product>.Empty).ToList();

            CategoryRepo tempRepo = new CategoryRepo(strConne, strDatab);
            foreach (var item in elenco)
            {
                item.InfoCategoria = tempRepo.GetById(item.Category);
            }

            return elenco;
            
        }

        public Product GetById(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Product t)
        {
            Product temp = prodotti.Find(d => d.codiceUniSku == t.codiceUniSku).FirstOrDefault();
            if (temp == null)
            {
                
                CategoryRepo tempRepo = new CategoryRepo(strConne, strDatab);
                Category tempCate = tempRepo.FindByTitle(t.categoria);

                if (tempCate != null)
                {
                    t.Category = tempCate.DocumentID;
                    prodotti.InsertOne(t);
                    if (t.DocumentID != null)
                        return true;
                }

            }

            return false;
        }

        public bool Update(Product t)
        {
            throw new NotImplementedException();
        }
    }
}
