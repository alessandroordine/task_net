﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_E_Commerce.Models;

namespace Task_E_Commerce.DAL
{
    public class CategoryRepo : InterfaceRepo<Category>
    {
        private IMongoCollection<Category> categorie;

        public CategoryRepo(string strConne, string strDatab)
        {
            var client = new MongoClient(strConne);
            var db = client.GetDatabase(strDatab);

            if (categorie == null)
                categorie = db.GetCollection<Category>("Categories");
        }
        public bool Delete(ObjectId varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetAll()
        {
            return categorie.Find(FilterDefinition<Category>.Empty).ToList();
        }

        public Category GetById(ObjectId varId)
        {
            return categorie.Find(d => d.DocumentID == varId).FirstOrDefault();
        }

        public bool Insert(Category t)
        {
            Category temp = categorie.Find(d => d.codice == t.codice).FirstOrDefault();
            if (temp == null)
            {
                categorie.InsertOne(t);
                if (t.DocumentID != null)
                    return true;
            }

            return false;
        }

        public bool Update(Category t)
        {
            throw new NotImplementedException();
        }

        public Category FindByTitle(string varNome)
        {
            return categorie.Find(d => d.nome == varNome).FirstOrDefault();
        }

}
