﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task_E_Commerce.Models
{
    public class Category
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [Required]
        [StringLength(50)]
        public string nome { get; set; }

        [Required]
        [StringLength(50)]
        public string scaffale { get; set; }

        [Required]
        [StringLength(150)]
        public string descrizione { get; set; }

        [StringLength(50)]
        public string codice { get; set; }
        
    }
}
