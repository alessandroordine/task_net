﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task_E_Commerce.Models
{
    public class Product
    {

        [BsonId]
        public ObjectId DocumentID { get; set; }

        [Required]
        [MaxLength(255)]
        public string nome { get; set; }

        [Required]
        [MaxLength(255)]
        public string descrizione { get; set; }

        [Required]
        public float prezzo { get; set; }

        public ObjectId Category { get; set; }

        [Required]
        [BsonIgnore]
        public string categoria { get; set; }

        [BsonIgnore]
        public Category InfoCategoria { get; set; }

        [Required]
        public int quantita { get; set; }

        [Required]
        [MaxLength(255)]
        public string codiceUniSku { get; set; }

       

      

        
    }
}
