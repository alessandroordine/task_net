﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_E_Commerce.DAL;
using Task_E_Commerce.Models;

namespace Task_E_Commerce.Controllers
{
    [ApiController]
    [Route("api/prodotti")]
    public class ProductController : Controller
    {
        private static ProductRepo _repository;
        public ProductController(IConfiguration configurazione)
        {
            if (_repository == null) 
            {
                bool isLocale = configurazione.GetValue<bool>("IsLocale");

                string stringaConnessione = isLocale == true ?
                     configurazione.GetValue<string>("MongoSettings:DatabaseLocale") :
                     configurazione.GetValue<string>("MongoSettings:ServerRemoto");

                string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");
                _repository = new ProductRepo(stringaConnessione, nomeDatabase);
             }
                
        }


        [HttpGet]
        public ActionResult Lista()
        {
            return Ok(_repository.GetAll());
        }


        [HttpPost("aggiungi")]
        public ActionResult Aggiungi(Product prodotto)
        {
            if (_repository.Insert(prodotto))
                return Ok(new { Status = "success" });
            else
                return Ok(new { Status = "error" });
        }

       
    }
}
