﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_E_Commerce.DAL;
using Task_E_Commerce.Models;

namespace Task_E_Commerce.Controllers
{
    [ApiController]
    [Route("api/categorie")]
    public class CategoryController : Controller
    {
        private readonly CategoryRepo _repository;
        public CategoryController(IConfiguration configurazione)
         {
                       
             bool isLocale = configurazione.GetValue<bool>("IsLocale");

             string stringaConnessione = isLocale == true ?
                     configurazione.GetValue<string>("MongoSettings:DatabaseLocale") :
                     configurazione.GetValue<string>("MongoSettings:ServerRemoto");


             string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");
             _repository = new CategoryRepo(stringaConnessione, nomeDatabase);
           
        }


        [HttpPost("aggiungi")]
        public ActionResult Aggiungi(Category cat)
        {
            cat.codice = Guid.NewGuid().ToString();

            if (_repository.Insert(cat))
                return Ok(new { Status = "success"});

            return Ok(new { Status = "error"});
        }

        [HttpGet]
        public ActionResult Lista()
        {
            return Ok(_repository.GetAll());
        }
    }
}

   
