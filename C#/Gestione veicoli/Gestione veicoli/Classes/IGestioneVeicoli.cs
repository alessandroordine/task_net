﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gestione_veicoli.Classes
{
    public interface IGestioneVeicoli
    {
        void drive();
        void tipoMovimento();
        void inserisciMoto();
        void inserisciAutomobile();
    }
}
