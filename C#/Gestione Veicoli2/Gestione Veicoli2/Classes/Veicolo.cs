﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gestione_Veicoli2.Classes
{
    public abstract class Veicolo
    {
        protected string NumeroTelaio { get; set; }

        private double velocita = 0;
        protected double velocitaMax = 100;
        public void accelera(double varVelocitaFinale)
        {
            if(varVelocitaFinale > 0)
            {
                velocita = varVelocitaFinale;
            }
           
        }
    }
}
