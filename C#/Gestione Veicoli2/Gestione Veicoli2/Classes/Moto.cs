﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gestione_Veicoli2.Classes
{
    public class Moto : Veicolo
    {
        public int NumRuote { get; set; }
        public Moto(string varTelaio, int varRuote)
        {
            NumeroTelaio = varTelaio;
            NumeroRuote = varRuote;
        }


    }

    public override string ToString()
        {
        return $"Moto {NumeroTelaio} - Porte: {NumeroPorte} - {velocitaMax}";
        }
    }
}
