﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gestione_Veicoli2.Classes
{
    public class Automobile : Veicolo
    {
        public int NumeroPorte { get; set; }
        public Automobile(string varTelaio, int varPorte)
        {
            NumeroTelaio = varTelaio;
            NumeroPorte = varPorte;
        }

        public void setVelocitaMassima(int varVelMax)
        {
            velocitaMax = varVelMax;
        }

        public override string ToString()
        {
            return $"{NumeroTelaio} - Porte: {NumeroPorte} - {velocitaMax}";
       }
    }
}
