﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EsercizioGio2.Classes
{
    public class Studente
    {

        public string Nominativo { get; set; }
        
        public string Citta { get; set; }
        public int Cfu;
        private int cfu;

        public int cfu
        {
            get { return ccfu; }
            set { 
                if (cfu = value; }
        }


        private int matricola;

        public int Matricola
        {
            get { return matricola; }
            set { matricola = value; }
        }

        public Studente()
        {

        }
        public Studente(string varNominativo,int varMatricola, string varCitta, int varCfu)
        {
            Nominativo = varNominativo;
            Matricola = varMatricola;
            Citta = varCitta;
            Cfu = varCfu;
        }
        public override string ToString()
        {
            string annoIscrizione = "N.D.";
            if (Cfu >= 0 && Cfu <= 60)
                annoIscrizione = "Primo anno";
            else if (Cfu > 60 && Cfu < 120)
                annoIscrizione = "Secondo anno";
            else if (Cfu >= 120)
                annoIscrizione = "Terzo anno";

            return $"Nome: {Nominativo} - Matricola: {Matricola} - Città: {Citta} - Cfu: {Cfu} - Anno: {annoIscrizione}";
        }

        public string verificaAnno(int varCfu)
        {
            string annoIscrizione = "N.D.";
            if (Cfu >= 0 && Cfu <= 60)
                annoIscrizione = "Primo anno";
            else if (Cfu > 60 && Cfu < 120)
                annoIscrizione = "Secondo anno";
            else if (Cfu >= 120)
                annoIscrizione = "Terzo anno";

            return annoIscrizione;
        }
    }
}
