﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Task_libri.Models
{
    [Table("ListaOggetti")]
    [Index(nameof(Codice), IsUnique = true)]
    public class Oggetto
    {
        [Key]

        public int Id { get; set; }

        [MaxLength(250)]
        [Required]
        public string Nome { get; set; }

        [MaxLength(500)]
        public string Descrizione { get; set; }

        [MaxLength(50)]
        [Required]
        public string Codice { get; set; }


        [MaxLength(50)]
        [Required]
        public int Quantita { get; set; }
    }
}
