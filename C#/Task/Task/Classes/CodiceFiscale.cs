﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task.Classes
{
    public class CodiceFiscale
    {
        public string Codice { get; set; }
        public string DataScadenza { get; set; }

        public override string ToString()
        {
            string scadenza = DataScadenza.ToString("yyyy-MM-dd");

            return $"{Codice} scade: {scadenza}";
        }

    }
}
