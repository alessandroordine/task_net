﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task.Classes
{
    public class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public CodiceFiscale CodFis { get; set; }

        public CartaIdentita CarIde { get; set; }

        public override string ToString()
        {
            return $"{Nome} {Cognome}\n" +
                $"{CodFis}\n" +
                $"{CarIde}";
        }
    }
}
