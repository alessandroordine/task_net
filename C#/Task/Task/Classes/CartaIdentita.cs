﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task.Classes
{
    public class CartaIdentita
    {
        public string Codice { get; set; }
        public DateTime DataEmissione { get; set; }
        public DateTime DataScadenza { get; set; }

        private string emissione;

        public string Emissione
        {
            get { return emissione; }
            set {
                if (value.Equals("Comune") || value.Equals("Zecca dello stato"))
                    emissione = value;
                else
                    Console.WriteLine("errore, parametro non consentito!\n" +
                       "Paramentri possibili slo: Comune!Zecca");
            }
        }


        public override string ToString()
        {
            string scadenza = DataScadenza.ToString("yyyy-MM-dd");
            string emissione = DataScadenza.ToString("yyyy-MM-dd");

            return $"carta identita: {Codice},\n" +
                $"emessa il: {emissione},\n" +
                $"scadenza il: {scadenza} - emessa da: { Emissione}";
        }
    }
}
