﻿using System;
using Task.Classes;

namespace Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona alessandro = new Persona();
            alessandro.Nome = "Alessandro";
            alessandro.Cognome = "Ordine";
            alessandro.CodFis = new CodiceFiscale()
            {
                Codice = "RDNLSN",
                DataScadenza = "2024-05-06"

            };

            CartaIdentita CarIde = new CartaIdentita() 
            { 
                Codice = "123456",
                DataScadenza = new DateTime(2000, 08,09),
                DataEmissione = new DateTime(2034, 54, 54),
                Emissione = "Zecca dello stato"
            };

            Console.WriteLine(alessandro);
            
        }
    }
}
