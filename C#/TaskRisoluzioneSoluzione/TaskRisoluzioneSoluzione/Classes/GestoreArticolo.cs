﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskRisoluzioneSoluzione.Classes
{
    class GestoreArticolo
    {
        private List<Articolo> elenco = new List<Articolo>();

        public List<Articolo> ElencoArticoli
        {
            get { return elenco; }
            set { elenco = value; }
        }

        public bool inserisciArticolo(string varCodice, string varNome, double varPrezzo, bool varNoSugar)
        {
            Bevanda temp = new Bevanda()
            {
                Nome = varNome,
                Codice = varCodice,
                IsNoSugar = varNoSugar,
                Prezzo = varPrezzo
            };

            elenco.Add(temp);

            return true;
        }



        public bool inserisciArticolo(string varCodice, string varNome, double varPrezzo, string varCondimento)
        {
            Piatto temp = new Piatto()
            {
                Nome = varNome,
                Codice = varCodice,
                Condimento = varCondimento,
                Prezzo = varPrezzo
            };

            elenco.Add(temp);

            return true;

        }
    }
}
