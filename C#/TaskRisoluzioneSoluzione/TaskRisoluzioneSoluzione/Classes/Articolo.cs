﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskRisoluzioneSoluzione.Classes
{
    public abstract class Articolo
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public double Prezzo { get; set; }

        public override string ToString()
        {
            return $"{Codice}{Nome}{Prezzo}";
        }
    }
}
