﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskRisoluzioneSoluzione.Classes
{
    public class Bevanda: Articolo
    {
        public bool IsNoSugar { get; set; }

        public override string ToString()
        {
            return $"(base.ToString() IsNoSugar:{IsNoSugar}";
        }
    }
}
