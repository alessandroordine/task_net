﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskRisoluzioneSoluzione.Classes
{
    public class Piatto: Articolo
    {
        public string Condimento { get; set; }

        public override string ToString()
        {
            return $"(base.ToString() Condimento:{Condimento}";
        }
    }
}
