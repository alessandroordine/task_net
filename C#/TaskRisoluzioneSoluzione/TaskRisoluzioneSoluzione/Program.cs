﻿using System;
using TaskRisoluzioneSoluzione.Classes;

namespace TaskRisoluzioneSoluzione
{
    class Program
    {
        static void Main(string[] args)
        {
            Bevanda cola = new Bevanda()
            {
                Nome = "Coca Cola",
                Codice = "C012456",
                Prezzo = 124.9f,
                IsNoSugar= true
                    
            };
            Console.WriteLine(cola);
        }
    }
}
