﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_Libreria.Models;

namespace Task_Libreria.Data
{
    public class LibriRepo
    {
        private readonly DatabaseContext _context;

        public LibriRepo(DatabaseContext con)
        {
            _context = con;
        }

        public bool Insert(Libro t)
        {
            try
            {
                _context.ElencoLibri.Add(t);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
