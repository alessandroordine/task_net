﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Libreria.Data
{
    public interface InterfaceRepo<T>
    {
        bool Insert(T t);

        IEnumerable<T> GetAll();

        T GetById(int varId);

        bool Update(T t);

        bool Delete(int varId);

        T FindByCode(string varCodice);
    }
}
