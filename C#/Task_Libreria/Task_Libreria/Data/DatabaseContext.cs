﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_Libreria.Models;

namespace Task_Libreria.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> opzioni) : base(opzioni)
        {

        }

        public DbSet<Libro> ElencoLibri { get; set; }
    }
}
