﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task_Libreria.Data;
using Task_Libreria.Models;

namespace Task_Libreria.Controllers
{
    [ApiController]
    [Route("api/oggetti")]
    public class LibroController : Controller
    {
        private readonly InterfaceRepo<Libro> _repository;

        public LibroController(InterfaceRepo<Libro> rep)
        {
            _repository = rep;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Libro>> RestituisciLibri()
        {
            var elenco = _repository.GetAll();

            return Ok(elenco);
        }

        [HttpPost("inserisci")]             
        public ActionResult<Riscontro> InserisciLibro(Libro objLibro)
        {
            Libro temp = _repository.FindByCode(objLibro.Codice);

            if (temp == null)
            {
                if (_repository.Insert(objLibro))
                    return Ok(new Riscontro() { Status = "success", Descrizione = "" });
                else
                    return Ok(new Riscontro() { Status = "error", Descrizione = "Operazione non riuscita!" });
            }
            else
            {
                temp.Quantita += objLibro.Quantita;

                if (_repository.Update(temp))
                    return Ok(new Riscontro() { Status = "success", Descrizione = "" });
                else
                    return Ok(new Riscontro() { Status = "error", Descrizione = "Operazione non riuscita!" });
            }


           
    }
}
