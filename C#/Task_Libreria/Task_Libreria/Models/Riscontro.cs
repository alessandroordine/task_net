﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Libreria.Models
{
    public class Riscontro
    {
        public string Status { get; set; }
        public string Descrizione { get; set; }
    }
}
