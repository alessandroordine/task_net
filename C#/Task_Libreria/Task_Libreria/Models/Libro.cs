﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Libreria.Models
{
    public class Libro
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Nome { get; set; }

        [MaxLength(500)]
        public string Descrizione { get; set; }

        [Required]
        [MaxLength(25)]
        public string Codice { get; set; }

        [Required]
        public int Quantita { get; set; }

        
    }
}
