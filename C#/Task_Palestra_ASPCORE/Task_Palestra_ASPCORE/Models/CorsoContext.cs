﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Palestra_ASPCORE.Models
{
    public class CorsoContext : DbContext
    {
        public CorsoContext(DbContextOptions<CorsoContext>options) : base(options)
        {

        }

        
        public DbSet<Corso> elencoCorsi { get; set; }
    }
}
