﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Palestra_ASPCORE.Models
{
    [Table("Utenti")]
    public class Utente
    {
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [StringLength(50)]
        public string Cognome { get; set; }
        [Required]
        [StringLength(150)]
        public string Indirizzo { get; set; }
        [Required]
        public DateTime DataNascita { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}


//CREARE UN SISTEMA DI GESTIONE PALESTRA CHE PERMETTA AGLI UTENTI DI ISCRIVERSI AD UN CORSO.

//L'elenco dei corsi sarà già presente in Database e sarà composto da:
//- Titolo corso
//- Descrizione breve
//- Codice univoco
//- Data e Ora

//Un utente, dall'esterno, potrà consultare i corsi senza essere autenticato ma la registrazione sarà necessariamente subordinata al Login. Non permetterò quindi l'iscrizione ad un corso se non sono prima autenticato.


//Nota bene: Gli utenti non saranno già presenti nel Database ma potranno iscriversi tramite un'apposita pagina.

//Una volta autenticato, oltre a poter consultare i corsi, iscrivermi e cancellare una eventuale iscrizione, sarà presente anche una pagina con l'elenco dei corsi a cui sono iscritto.

//Nel form di iscrizione utente sarano presenti i seguenti campi:
//-Nome
//- Cognome
//- Indirizzo
//- DataNascita
//- Username(email)
//- Password
//- *Conferma Password

//- SCEGLIERE TRA SQL E MONGO
