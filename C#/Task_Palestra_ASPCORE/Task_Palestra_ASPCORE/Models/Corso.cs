﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Palestra_ASPCORE.Models
{
    public class Corso
    {
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string Codice { get; set; }
        public DateTime DataOra { get; set; }
    }
}
