﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Mongo_API_Prodotti_TASK.Models
{
    public class Category
    {
        [BsonId]
        public ObjectId DocumentId { get; set; }
        [Required, MaxLength(150)]
        public string Titolo { get; set; }
        [Required, MaxLength(150)]
        public string Scaffale { get; set; }
        [Required, MaxLength(150)]
        public string Descrizione { get; set; }
        [MaxLength(150)]
        public string Codice { get; set; }
    }
}
