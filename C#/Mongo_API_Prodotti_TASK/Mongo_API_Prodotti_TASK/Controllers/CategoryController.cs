﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Mongo_API_Prodotti_TASK.DAL;
using Mongo_API_Prodotti_TASK.Models;
using MongoDB.Bson;

namespace Mongo_API_Prodotti_TASK.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : Controller
    {
        private static CategoryRepo _repo;

        public CategoryController(IConfiguration config)
        {
            if (_repo == null)
            {
                bool isLocale = config.GetValue<bool>("IsLocale");

                string connString = isLocale
                    ? config.GetValue<string>("MongoSettings:DatabaseLocale")
                    : config.GetValue<string>("MongoSettings:DatabaseRemoto");

                string nomeDb = config.GetValue<string>("MongoSettings:NomeDatabase");
                _repo = new CategoryRepo(connString, nomeDb);
            }
        }

        [HttpGet]
        public ActionResult GetAllCategories() => Ok(_repo.GetAll());

        [HttpGet("{varId")]
        public ActionResult GetCategoryById(string varId) => Ok(_repo.GetById(ObjectId.Parse(varId)));

        [HttpPost("insert")]
        public ActionResult InsertCategory(Category cate)
        {
            if (_repo.Insert(cate))
                return Ok(new { Status = "success" });
            
            return Ok(new { Status = "error" });
        }

        [HttpDelete("{varId}")]
        public ActionResult DeleteCategory(string varId)
        {
            if (_repo.Delete(ObjectId.Parse(varId)))
                return Ok(new { Status = "success" });
            
            return Ok(new { Status = "error" });
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyCategory(string varId, Category cate)
        {
            cate.DocumentId = ObjectId.Parse(varId);
            if (_repo.Update(cate))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }
    }
}
