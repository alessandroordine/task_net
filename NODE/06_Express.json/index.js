const express = require('express')
const app = express()

// const indirizzo = '127.0.0.1' se non la metti è in default su local
const porta = 4001

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

//ROTTE
app.get("/", (req,res) => {
    let persona = {
        nome: "Giovanni",
        cognome: "Pace",
        eta: 35
    }

    res.json(persona);
})

app.get("/persona", (req,res) => {
    let persona = {
        nome: "Giovanni",
        cognome: "Pace",
        eta: 35
    }
    res.status(400).json(null);
})

app.get("/lista", (req,res) => {
    let elenco = [
        {
        nome: "Giovanni",
        cognome: "Pace",
        },
        {
            nome: "Mario",
            cognome: "Pace", 
        }
        
    ]

})

