const http = require("http");
const { listenerCount } = require("process");

const indirizzo = "127.0.0.1"  //indirizzo = Host 
const porta = 4001


const server = http.createServer((req, res) => {

    // console.log(req.url);
    // console.log(req.method);

    let responso = {
        urlRiferimento: req.url,
        metodoRichiesto: req.method

    }
    res.statusCode = 200;
    res.setHeader("Content-Type", "txt/json")
    res.end(JSON.stringify(responso)); 

    // console.log("Richiesta")
})

server.listen(porta, indirizzo, () => {
    console.log("Sono in ascolto sulla porta ${indirizzo} :${porta}") //"" devono essere gravi;
})