const http = require("http");
const fs = require("fs");    // Modulo FileSystem

const indirizzo = "127.0.0.1"  //indirizzo = Host 
const porta = 4001

const homePage = fs.readFileSync('assets/home.html');
const contPage = fs.readFileSync('assets/contatti.html');
const servPage = fs.readFileSync('assets/servizi.html');
const erroPage = fs.readFileSync('assets/errore.html');


const server = http.createServer((req, res) => {
    switch(req.url){
        case "/":
            res.end(homePage);
            break;
            case "/contatti":
                res.end(contPage);
                break
            case "/servizi":
                res.end(servPage);
                break
                default:
                    res.statusCode = 404;
                    res.end(erroPage);
                    break
    }
})


server.listen(porta, indirizzo, () => {
    console.log(`Sono in ascolto sulla porta ${indirizzo}:${porta}`)            //"" devono essere gravi;
})