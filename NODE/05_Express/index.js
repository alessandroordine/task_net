const express = require('express')
const app = express()

// const indirizzo = '127.0.0.1' se non la metti è in default su local
const porta = 4001

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

//ROTTE
app.get("/", (req,res) => {
    res.end("Sono la Home")
})

app.get("/contatti", (req,res) => {
    res.end("Sono la Contatti")
})

app.get("/servizi", (req,res) => {
    res.end("Sono la servizi")
})

