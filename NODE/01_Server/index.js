const http = require("http");
const { listenerCount } = require("process");

const indirizzo = "127.0.0.1"  //indirizzo = Host 
const porta = 4001


const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.end("CIAO GIOVANNI"); 

    console.log("Richiesta")
})

server.listen(porta, indirizzo, () => {
    console.log("Sono in ascolto sulla porta " + porta)
})