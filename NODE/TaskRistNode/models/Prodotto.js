const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProdottoSchema = new Schema({
    nome: String,
    tipo: String,
    prezzo: Number
})